<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Auth;

class AuthController extends Controller
{
    public function attempt(Request $request){
        $email = $request->get('email');
        $password = $request->get('password');
        $device_token = $request->get('deviceToken');
//        dd($request->all());

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            $user = Auth::user();
            $user->device_token = $device_token;
            $user->save();
            return response()->json($user);
        } else {
            return response()->json(false);
        }
    }
}
