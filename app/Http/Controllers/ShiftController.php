<?php

namespace App\Http\Controllers;

use App\Push;
use Illuminate\Http\Request;
use App\Shift;
use App\Category;
use App\User;

class ShiftController extends Controller
{
    public function getAll(){
        return response()->json(Shift::with('user', 'category', 'acceptee')->latest()->get());
    }

    public function getNonAccepted(){
        return response()->json(Shift::with('user', 'category', 'acceptee')->whereNull('acceptee_id')->latest()->get());
    }

    public function getAccepted($userId){
        return response()->json(Shift::with('user', 'category', 'acceptee')->where('acceptee_id', '=', $userId)->latest()->get());
    }

    public function getPosted($userId){
        return response()->json(Shift::with('user', 'acceptee', 'category')
            ->where([
                ['user_id', '=', $userId],
//                ['start_date', '>', 'DATE(NOW())'],
            ])->latest()->get());
    }

    public function accept(Request $request){
//        try{
            $input = (Object) $request->all();
            if(isset($input->shift_id) && isset($input->acceptee_id)){
                $shift = Shift::find($input->shift_id);
                $user = User::find($input->acceptee_id);

                $shift->acceptee()->associate($user);
                $shift->save();
                return response()->json( array(
                    'success' => $shift
                ));
            } else {
                return response()->json( array(
                    'success' => false,
                    'message' => 'Invalid parameters'
                ), 500);
            }

//            $push = new Push();
//            $push->addDevice($shift->user->device_token);
//
//            return response()->json( array(
//                'success' => $shift->save()
//            ));
//        } catch(\Exception $ex){
//            dd($ex);
//            return response()->json( array(
//                'success' => false,
//                'message' => $ex->message
//            ));
//        }
    }

    public function show($id){
        $shift = Shift::with('user', 'category')->where('id', $id)->first();
        return response()->json($shift);
    }

    public function store(Request $request){
        try{
            $input = $request->all();
            $shift = new Shift();

//        dd(Category::find($input['category_id']));
//        dd(User::find($input['user_id']));
            $shift->start_date = $input['start_date'];
            $shift->end_date = $input['end_date'];
            if(isset($input['tip'])){
                $shift->tip = $input['tip'];
            }
            if(isset($input['comment'])){
                $shift->comment = $input['comment'];
            }

            $shift->category()->associate(Category::find($input['category_id']));
            $shift->user()->associate(User::find($input['user_id']));
//        dd($shift);

            $push = new Push();
            $push->addDevice($shift->user->device_token);

            return response()->json( array(
                'success' => $shift->save()
            ));
        } catch(\Exception $ex){
            return response()->json( array(
                'success' => false,
                'message' => $ex->message
            ));
        }

        try{
            $push->send("New shift");
        } catch(\Exception $ex){
//            dd($ex);
//            return response()->json( array(
//                'success' => false,
//                'message' => $ex->message
//            ));
        }


    }
}
