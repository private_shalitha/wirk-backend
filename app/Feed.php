<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    public function tags()
    {
        return $this->belongsToMany('App\User', 'feeds_user');
    }
}
