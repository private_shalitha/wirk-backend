<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \PushNotification;

class Push extends Model
{
    private $push;
    private $deviceArray = array();

    public function __construct()
    {
//        parent::__construct($attributes);
        $this->push = PushNotification::app('wirkIOS');
    }

    public function addDevice($deviceToken){
        array_push($this->deviceArray, PushNotification::Device($deviceToken));
    }

    public function send($message){
        // Populate the device collection
        $devices = PushNotification::DeviceCollection($this->deviceArray);

// Send the notification to all devices in the collect
        $this->push
            ->to($devices)
            ->send($message);
    }
}
