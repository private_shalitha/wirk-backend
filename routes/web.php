<?php

use \App\User;
use \App\Category;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/', function () {
//    \DB::statement('drop table shifts');
//    \DB::statement('drop table categories');
//    \DB::statement('drop table users');
//    \DB::statement('drop table migrations');
//    \DB::statement('drop table password_resets');
    return view('welcome');
});

Route::group(['middleware' => 'cors'], function () {
    Route::get('api/users', function () {
        return response()->json(User::all());
    });
    Route::get('api/users/{id}', function ($id) {
        return response()->json(User::find($id));
    });

    Route::get('api/users/{id}/survey', function ($id) {
        $user = User::find($id);
        $user->survey_submited = !$user->survey_submited;
        $user->save();
        return response()->json(array(
            'success' => $user
        ));
    });

    Route::get('api/categories', function () {
        return response()->json(Category::all());
    });

    Route::get('api/shifts', 'ShiftController@getAll');
    Route::get('api/shifts/non_accepted', 'ShiftController@getNonAccepted');
    Route::get('api/shifts/accepted/{id}', 'ShiftController@getAccepted');
    Route::get('api/shifts/posted/{id}', 'ShiftController@getPosted');
    Route::post('api/shifts/accept', 'ShiftController@accept');

    Route::get('api/shifts/{id}', 'ShiftController@show');
    Route::post('api/shifts', 'ShiftController@store');


    Route::post('api/login', 'AuthController@attempt');
});
