<?php

use Illuminate\Database\Seeder;
use \Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call("UserTableSeeder");
        $this->call("CategorySeeder");
//        $this->call("ShiftSeeder");
    }


}

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            'name' => 'Bill Barmer ',
            'email' => 'bill@gmail.com',
            'password' => bcrypt('123'),
            'avatar' => "main/assets/images/1.jpg"
        ]);

        DB::table('users')->insert([
            'name' => 'Anne Alexander',
            'email' => 'anne@gmail.com',
            'password' => bcrypt('123'),
            'avatar' => "main/assets/images/5.jpg"
        ]);

        DB::table('users')->insert([
            'name' => 'John Smith',
            'email' => 'john@gmail.com',
            'password' => bcrypt('123'),
            'avatar' => "main/assets/images/3.jpg"
        ]);

        DB::table('users')->insert([
            'name' => 'Tim Chandler',
            'email' => 'tim@gmail.com',
            'password' => bcrypt('123'),
            'avatar' => "main/assets/images/4.jpg"
        ]);
    }
}

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        DB::table('categories')->insert([ 'name' => 'Marketing' ]);
        DB::table('categories')->insert([ 'name' => 'Engineering' ]);
        DB::table('categories')->insert([ 'name' => 'Sales' ]);
    }
}
